from django.urls import path
from .views import *

app_name = "app"

urlpatterns = [
    path('index', IndexView.as_view(), name='index'),
    path('named/entities', NamedEntitiesView.as_view(), name='namedEntities'),
    path('common/words', CommonWordsView.as_view(), name='commonWords')
]