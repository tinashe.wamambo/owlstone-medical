from ..models import *


class DataController:

    def __init__(self):
        """ Constructor

        :return void
        """
        self.file = File()

    def getFileText(self):
        """ Return text extracted from file

        :return list
        """
        return self.file.getText()

    def getNamedEntities(self):
        """ Get named entities for each paragraph

        :return list
        """
        results = []

        for paragraph in self.file.getParagraphs():
            if paragraph.getNamedEntities():
                results.append({
                    'paragraph_text': paragraph,
                    'named_entities': paragraph.getNamedEntities()
                })

        return results

    def getCommonWords(self):
        """ Get commonly used words in the text

        :return list
        """
        return self.file.getCommonWords()