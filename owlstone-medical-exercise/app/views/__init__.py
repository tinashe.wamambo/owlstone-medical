from .index_view import IndexView
from .common_words_view import CommonWordsView
from .named_entities_view import NamedEntitiesView