from django.views import View
from django.shortcuts import render

from ..controllers import DataController


class CommonWordsView(View):

    def get(self, request):
        """ Get common words page

        :return render html page
        """
        return render(
            request, 
            'common_words.html',
            {'common_words': DataController().getCommonWords()}
        )